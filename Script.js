
/*array declaration and initialization*/
var dataSourceArray=[];
/* const tr=document.createElement("tr");it is table row object creation output is <tr></tr>*/

function onSave(){
alert("form submitted")
 /*It is getting element present in html file  by using id provided in html tag attribute and access the particular object by using properties and functions 
var dbtype=document.getElementById ("dbtype").value;
var url=document.getElementById("url").value;
var portnumber=document.getElementById("pn").value;
var username=document.getElementById ("un").value;
var password=document.getElementById ("ps").value;
*/
/*object creation*/
var dataSourceObj={
    dbtype:document.getElementById ("dbtype").value,
    host:document.getElementById("url").value,
    portnumber:document.getElementById("pn").value,
    username:document.getElementById ("un").value,
    password:document.getElementById ("ps").value,
}

/*adding object in array using methods of array (push())*/
dataSourceArray.push(dataSourceObj);

/*table-tbody-trow-td-value insertion*/
var htmltag=document.getElementById("dataSourceTableBody");
let updatedHtmlTag="";
/*forEach loop*/
dataSourceArray.forEach(element => {
    updatedHtmlTag=updatedHtmlTag+"<tr><td>"+element.dbtype+"</td><td>"+element.host+"</td><td>"+element.portnumber+"</td><td>"+element.username+"</td><td>"+element.password+"</td></tr>";
    htmltag.innerHTML=updatedHtmlTag;
});    

/*document.getElementById("result").innerHTML=dataSourceArray*/
}

function looping() {
    /*for loop*/
for (let index = 0; index < dataSourceArray.length; index++) {
    console.log(dataSourceArray[index]);  
};

/*for of*/
for (let each of dataSourceArray) {
    console.log(each);
};
}

function filter() {
    /*array filter to get needed objects*/
console.log(dataSourceArray.filter((each)=>{return each['dbtype']==='mysql';}));
}

function mapping() {
    /*array map method used to manipulate each object property*/
console.log(dataSourceArray.map((each)=>{each['host']="http://"+each['host']+":"+each['portnumber'];return each;}));
}